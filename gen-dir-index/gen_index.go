package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"strings"
)

func main() {
	pwd, _ := os.Getwd()
	fmt.Println(pwd)

	gen_index(pwd)
}

func gen_index(dir string) {
	fileInfoList, err := ioutil.ReadDir(dir)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println(len(fileInfoList))

	GenIndex(&fileInfoList, dir)
	for _, fileInfo := range fileInfoList {
		if strings.HasPrefix(fileInfo.Name(), ".") {
			continue
		}
		if fileInfo.IsDir() {
			gen_index(dir + "/" + fileInfo.Name())
		} else {
			if !strings.HasSuffix(fileInfo.Name(), ".html") {
				continue
			}
			fmt.Println(dir + "/" + fileInfo.Name())
		}
	}
}

func GenIndex(list *[]os.FileInfo, dir string) {
	fmt.Println("GenIndex: " + dir)
	index := dir + "/index.html"
	split := strings.Split(dir, "/")
	filename := split[len(split)-1]

	str := "<!DOCTYPE html>\n<html lang=\"zh-CN\">\n<head>" +
		"\n    <meta charset=\"UTF-8\">" +
		"\n    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">" +
		"\n    <meta http-equiv=\"X-UA-Compatible\" content=\"ie=edge\">" +
		"\n    <title>" +
		filename + "</title>\n</head>\n<body>\n"

	p := "<p><a href=\"%s\">%s</a></p>"
	for _, info := range *list {
		if strings.HasPrefix(info.Name(), ".") || info.Name() == "index.html" {
			continue
		}
		if !info.IsDir() && !strings.HasSuffix(info.Name(), ".html") {
			continue
		}
		fmt.Println(dir + "/" + info.Name())
		str = str + fmt.Sprintf(p, info.Name(), info.Name())
	}
	str = str + "\n</body>\n</html>"

	f, err := os.Create(index)
	defer f.Close()
	if err != nil {
		log.Fatal(err)
	}
	_, err = f.Write([]byte(str))
	if err != nil {
		log.Fatal(err)
	}
}
