package domain_checker

import (
	"fmt"
	"testing"
	"time"
)

func TestCheckDomain1(t *testing.T) {
	got, err := CheckDomain("abc.cn")
	fmt.Println(err)
	fmt.Println(got)


}

func TestCheckDomain(t *testing.T) {
	genDomain("", ".cn", 4)
	fmt.Println(len(domains))
	type args struct {
		domain string
	}
	type Test struct {
		name    string
		args    args
		want    bool
		wantErr bool
	}
	var tests []Test
	for _, domain := range domains {
		tests = append(tests, Test{
			name: domain,
			args: args{
				domain: domain,
			},
			want:    true,
			wantErr: false,
		})
	}
	for _, tt := range tests {
		time.Sleep(time.Duration(100) * time.Millisecond)

		t.Run(tt.name, func(t *testing.T) {
			got, err := CheckDomain(tt.args.domain)
			if (err != nil) != tt.wantErr {
				t.Errorf("CheckDomain() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("CheckDomain() got = %v, want %v", got, tt.want)
			}
		})
	}
}

const letters = "abcdefghijklmnopqrstuvwxyz1234567890"

var domains []string

func genDomain(str string, base string, limit int) {
	if len(str) > 3 {
		domains = append(domains, str+base)
	}
	if len(str) < limit {
		for _, l := range letters {
			genDomain(str+string(l), base, limit)
		}
	}
}
