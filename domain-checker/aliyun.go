package domain_checker

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
)

func CheckDomain(domain string) (bool, error) {

	url := "https://checkapi.aliyun.com/check/checkdomain?domain=" + domain + "&command=&token=Y7f804f307eafc7308521dadaef3bf2d9&ua=&currency=&site=&bid=&_csrf_token=&callback=jsonp_1596859533514_39103"
	method := "GET"

	client := &http.Client{
	}
	req, err := http.NewRequest(method, url, nil)

	if err != nil {
		fmt.Println(err)
	}
	req.Header.Add("Cookie", "JSESSIONID=X3666391-J75I4TF82N2TNBUOOAAM2-UV9Z4LDK-8IUZ1; tmp0=c8WhVh5Avk6gEEWwyjscN0De3Su75Ojb47FN3ud7FjHdljYaFp2RB0gBaaWsz8zy%2FuIsc%2B1LSzMJEIox7GWNYt1%2Beoop1aa3eJ4hK%2FTQ5H4er%2BqYsCNNBOgUl9dr4dqALWvg3Nz5ZEbG6GevCO1U8g%3D%3D")

	res, err := client.Do(req)
	defer res.Body.Close()
	body, err := ioutil.ReadAll(res.Body)
	s := string(body)
fmt.Println(s)
	return (!strings.Contains(s, "preRegisterPrice")) && strings.Contains(s, "\"avail\":1,"), nil
}
