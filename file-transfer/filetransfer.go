package main

import (
	"html/template"
	"io"
	"log"
	"net/http"
	"os"
	"time"
)

var BaseUploadPath = "."

var (
	htmlTplEngine    *template.Template
	htmlTplEngineErr error
)

type FileInfo struct {
	Name      string
	Size      int64
	IsDir     bool
	CreatedAt time.Time
}

func init() {
	// 初始化模板引擎 并加载各层级的模板文件
	// 注意 views/* 不会对子目录递归处理 且会将子目录匹配 作为模板处理造成解析错误
	// 若存在与模板文件同级的子目录时 应指定模板文件扩展名来防止目录被作为模板文件处理
	// 然后通过 view/*/*.html 来加载 view 下的各子目录中的模板文件
	htmlTplEngine = template.New("htmlTplEngine")

	// 模板根目录下的模板文件 一些公共文件
	_, htmlTplEngineErr = htmlTplEngine.ParseGlob("views/*.html")
	if nil != htmlTplEngineErr {
		log.Panic(htmlTplEngineErr.Error())
	}
	// 其他子目录下的模板文件
	//_, htmlTplEngineErr = htmlTplEngine.ParseGlob("views/*/*.html")
	//if nil != htmlTplEngineErr {
	//	log.Panic(htmlTplEngineErr.Error())
	//}
}

func handleIndexPage(w http.ResponseWriter, r *http.Request) {
	if !isHttpMethodTypeAllowed(w, r, http.MethodGet) {
		return
	}
	log.Println(r.RequestURI)
	if r.RequestURI != "/" {
		return
	}

	baseDir, err := os.Open(BaseUploadPath)
	if err != nil {
		_, _ = io.WriteString(w, "Open dir error")
		return
	}
	defer baseDir.Close()
	fileInfos, err := baseDir.Readdir(10)
	if err != nil {
		_, _ = io.WriteString(w, "Read dir error")
		return
	}
	fileList := make([]FileInfo, len(fileInfos))
	for i, fileInfo := range fileInfos {
		fileList[i] = FileInfo{
			Name:      fileInfo.Name(),
			Size:      fileInfo.Size(),
			IsDir:     fileInfo.IsDir(),
			CreatedAt: fileInfo.ModTime(),
		}
	}

	_ = htmlTplEngine.ExecuteTemplate(
		w,
		"index",
		map[string]interface{}{
			"PageTitle": "Index",
			"FileList":  fileList,
		},
	)
}

func handleUpload(w http.ResponseWriter, r *http.Request) {
	if !isHttpMethodTypeAllowed(w, r, http.MethodPost) {
		return
	}

	file, fileHeader, err := r.FormFile("file")
	if err != nil {
		log.Println(err)
		_, _ = io.WriteString(w, "Read file error")
		return
	}
	defer file.Close()
	log.Println("upload: " + fileHeader.Filename)

	filename := BaseUploadPath + "/" + fileHeader.Filename
	if fileExists(filename) {
		log.Println("File exists " + filename)
		io.WriteString(w, "File exists ")
		return
	}
	newFile, err := os.Create(filename)
	if err != nil {
		log.Println(err)
		io.WriteString(w, "Create file error")
		return
	}
	defer newFile.Close()

	_, err = io.Copy(newFile, file)
	if err != nil {
		log.Println(err)
		io.WriteString(w, "Write file error")
		return
	}

	_, _ = io.WriteString(w, "Upload success.")
}

func handleDownload(w http.ResponseWriter, r *http.Request) {
	if !isHttpMethodTypeAllowed(w, r, http.MethodGet) {
		return
	}

	filename := r.FormValue("filename")
	if filename == "" {
		w.WriteHeader(http.StatusBadRequest)
		io.WriteString(w, "Bad request")
		return
	}
	filename = BaseUploadPath + "/" + filename
	log.Println("download: " + filename)

	if !fileExists(filename) {
		log.Println("File not exists " + filename)
		io.WriteString(w, "File not exists ")
		return
	}
	file, err := os.Open(filename)
	if err != nil {
		log.Println(err)
		io.WriteString(w, "Open file error")
		return
	}
	defer file.Close()

	w.Header().Add("Content-Type", "application/octet-stream")
	w.Header().Add("Content-Disposition", "attachment; filename=\""+filename+"\"")
	_, err = io.Copy(w, file)
	if err != nil {
		log.Println(err)
		io.WriteString(w, "Transfer file error")
		return
	}
}

func isHttpMethodTypeAllowed(w http.ResponseWriter, r *http.Request, methodType string) bool {
	if r.Method != methodType {
		w.WriteHeader(http.StatusMethodNotAllowed)
		_, _ = w.Write([]byte("Method not allowed."))
		return false
	}
	return true
}

func fileExists(filename string) bool {
	if _, err := os.Stat(filename); !os.IsNotExist(err) {
		return true
	}
	return false
}

func main() {
	http.HandleFunc("/upload", handleUpload)
	http.HandleFunc("/download", handleDownload)
	http.HandleFunc("/", handleIndexPage)

	err := http.ListenAndServe(":8000", nil)
	if err != nil {
		log.Fatal("Server start fail")
		return
	}
}
