package main

import "fmt"

func quicksort(arr []int32) []int32 {
	// 小于2的数组无需排序直接返回
	if len(arr) < 2 {
		return arr
	}
	// 基准值
	pivot := arr[0]
	// 小于基准值的子集
	var smaller []int32
	// 大于基准值的子集
	var larger []int32
	for _, i := range arr {
		if i > pivot {
			larger = append(larger, i)
		} else if i < pivot {
			smaller = append(smaller, i)
		}
	}
	// return [smaller_sorted] + pivot + [larger_sorted]
	a1 := append(quicksort(smaller), pivot)
	return append(a1, quicksort(larger)...)
}

func main() {
	arr := []int32{9, 8, 6, 2, 5, 4, 7}

	sorted := quicksort(arr)
	fmt.Println(sorted)
}
