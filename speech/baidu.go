package speech

import (
	"bytes"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"path/filepath"
	"strings"
)

type AsrReq struct {
	// 语音文件的格式，pcm/wav/amr/m4a。不区分大小写。推荐pcm文件
	Format  string `json:"format"`
	Rate    int    `json:"rate"`
	Channel int    `json:"channel"`
	CUid    string `json:"cuid"`
	Token   string `json:"token"`
	DevPid  string `json:"dev_pid"`
	LmId    string `json:"lm_id"`
	Speech  string `json:"speech"`
	Len     int    `json:"len"`
}

// filename: 录音文件名，支持录音文件格式 pcm/wav/amr/m4a
func NewAsr(filename, token string) *AsrReq {
	fileBytes, err := ioutil.ReadFile(filename)
	if err != nil {
		panic(err)
	}
	encodeToString := base64.StdEncoding.EncodeToString(fileBytes)
	ext := filepath.Ext(filename)
	if strings.HasPrefix(ext, ".") {
		ext = ext[1:]
	}

	return &AsrReq{
		Format:  ext,
		Rate:    16000,
		Channel: 1,
		CUid:    "1234gogo",
		Token:   token,
		Speech:  encodeToString,
		Len:     len(fileBytes),
	}
}

type Result struct {
	ErrNo  int      `json:"err_no"`
	ErrMsg string   `json:"err_msg"`
	SN     string   `json:"sn"`
	Result []string `json:"result"`
}

func Asr(asr *AsrReq) (string, error) {
	url := "http://vop.baidu.com/server_api"
	method := "POST"

	b, err2 := json.Marshal(asr)
	if err2 != nil {
		return "", err2
	}
	reader := bytes.NewReader(b)

	resultBody, err := Req(method, url, reader)
	if err != nil {
		return "", err
	}

	var result Result
	err = json.Unmarshal(*resultBody, &result)
	if err != nil {
		return "", err
	}
	if result.ErrNo > 0 {
		return "", fmt.Errorf("error - %d - %s", result.ErrNo, result.ErrMsg)
	}

	return result.Result[0], nil
}

func Req(method, url string, reader io.Reader) (*[]byte, error) {
	req, err := http.NewRequest(method, url, reader)

	if err != nil {
		return nil, err
	}
	req.Header.Set("Content-Type", "application/json; charset=utf-8")
	client := &http.Client{}
	resp, err := client.Do(req)
	defer resp.Body.Close()
	if err != nil {
		return nil, err
	}

	resultBody, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	return &resultBody, nil
}
