package speech

import (
	"fmt"
	"testing"
)

func TestToken(t *testing.T) {
	req := &TokenReq{
		ClientId:     "UYKEMeONkUNQVRdgQIHEX7ICVp8TGXKC",
		ClientSecret: "F2OpqtsO50veyYVoplkiGCwO9tSfOG9z",
		GrantType:    "client_credentials",
	}
	token, err := FetchToken(req)
	if err != nil {
		panic(err)
	}
	if token == "" {
		panic("fetch token fail")
	}
	fmt.Println(token)
}

func TestAsr(t *testing.T) {
	//filename := "/home/zhouyu/dev/code/demo/speech-demo/rest-api-asr/python/audio/16k.pcm"
	filename := "/home/zhouyu/音乐/record/demo.pcm"
	token := "24.5c305b47222218f9262792a1ce04e298.2592000.1597759484.282335-15803531"

	asr := NewAsr(filename, token)

	result, err := Asr(asr)
	if err != nil {
		panic(err)
	}
	fmt.Println(result)
}
