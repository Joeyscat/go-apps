package speech

import (
	"encoding/json"
	"fmt"
)

type TokenReq struct {
	ClientId     string `json:"client_id"`
	ClientSecret string `json:"client_secret"`
	GrantType    string `json:"grant_type"`
}

type TokenResp struct {
	RefreshToken  string `json:"refresh_token"`
	ExpiresIn     int    `json:"expires_in"`
	Scope         string `json:"scope"`
	SessionKey    string `json:"session_key"`
	SessionSecret string `json:"session_secret"`
	AccessToken   string `json:"access_token"`
}

type FetchTokenError struct {
	Error            string `json:"error"`
	ErrorDescription string `json:"error_description"`
}

func FetchToken(tokenReq *TokenReq) (string, error) {
	url := "http://openapi.baidu.com/oauth/2.0/token"
	method := "POST"

	url = url + "?client_id=" + tokenReq.ClientId + "&client_secret=" +
		tokenReq.ClientSecret + "&grant_type=" + tokenReq.GrantType

	resultBody, err := Req(method, url, nil)
	if err != nil {
		return "", err
	}

	var result TokenResp
	err = json.Unmarshal(*resultBody, &result)
	if err != nil {
		return "", err
	}
	if result.AccessToken == "" {
		var result FetchTokenError
		err = json.Unmarshal(*resultBody, &result)
		return "", fmt.Errorf("error - %s - %s", result.Error, result.ErrorDescription)
	}

	return result.AccessToken, nil
}
